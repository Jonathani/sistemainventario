"""inventario URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from inventarioapp import views
from django.contrib.auth.views import LoginView, logout_then_login
from django.conf.urls import handler400

urlpatterns = [
    path('admin/', admin.site.urls),
    path('login/', views.ingresarLogin, name='login'),
    path('logout/', views.logout_view, name='logout_view'),
    path('index/', views.index , name='index'),    
    path('clientes/', views.listarclientes, name='clientes'),
    path('clientes/<int:pk>/', views.ClienteDetailView.as_view(), name='detail'),
    path('clientes/edit/<int:pk>/', views.edit, name='edit'),
    path('clientes/create/', views.create, name='create'),
    path('clientes/delete/<int:pk>/', views.delete, name='delete'),
    path('bodega/', views.listarbodegas, name='bodega'),
    path('bodega/<int:pk>/', views.BodegaDetailView.as_view(), name='detailbodega'),
    path('bodega/create/', views.createBodega, name='createbodega'),
    path('bodega/edit/<int:pk>/', views.editBodega, name='editbodega'),
    path('bodega/delete/<int:pk>/', views.deleteBodega, name='deletebodega'),
    path('categoria/', views.listarcategorias, name='categoria'),
    path('categoria/create/', views.createCategoria, name='createcategoria'),
    path('categoria/<int:pk>/', views.CategoriaDetailView.as_view(), name='detailcategoria'),
    path('categoria/edit/<int:pk>/', views.editCategoria, name='editcategoria'),
    path('categoria/delete/<int:pk>/', views.deleteCategoria, name='deletecategoria'),    
    path('productos/', views.listarProductos, name='productos'),
    path('cproductos/', views.createProducto, name='cproductos'),
    path('cclientes/', views.createCliente, name='cclientes'),
    path('cbodega/', views.createBodega, name='cbodega'),
    path('ccategoria/', views.createCategoria, name='ccategoria'),
    path('productos/delete/<int:pk>/', views.deleteProducto, name='deleteproducto'),
]

handler400 = views.error404
