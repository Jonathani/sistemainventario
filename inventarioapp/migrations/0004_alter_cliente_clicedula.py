# Generated by Django 3.2.4 on 2021-06-21 02:51

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('inventarioapp', '0003_producto_clienteid'),
    ]

    operations = [
        migrations.AlterField(
            model_name='cliente',
            name='clicedula',
            field=models.CharField(max_length=20),
        ),
    ]
