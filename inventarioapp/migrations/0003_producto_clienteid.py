# Generated by Django 3.2.4 on 2021-06-21 02:44

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('inventarioapp', '0002_usuario'),
    ]

    operations = [
        migrations.AddField(
            model_name='producto',
            name='clienteid',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='inventarioapp.cliente'),
        ),
    ]
