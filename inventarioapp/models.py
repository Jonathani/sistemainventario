from django.db import models

class Bodega(models.Model):
    bogcodigo = models.CharField(max_length=250)
    bognombre = models.CharField(max_length=250)
    bogdireccion = models.CharField(max_length=250)

class Categoria(models.Model):
    catnom = models.CharField(max_length=250)

class Cliente(models.Model):
    clicedula = models.CharField(max_length=20)
    clinombre = models.CharField(max_length=250)
    cliapellido = models.CharField(max_length=250)
    clidireccion = models.CharField(max_length=250)
    clitelefono = models.CharField(max_length=250)
    clicorreo = models.CharField(max_length=250)

class Producto(models.Model):
    catid = models.ForeignKey(Categoria, on_delete=models.PROTECT)
    bogid = models.ForeignKey(Bodega, on_delete=models.PROTECT)
    clienteid = models.ForeignKey(Cliente, on_delete=models.PROTECT)
    procodigo = models.IntegerField()
    prodnombre = models.CharField(max_length=250)
    prodcantidad = models.IntegerField()
    preciocompra = models.FloatField()
    preciodeventa = models.FloatField()
    prodimagen = models.CharField(max_length=250)
    prodtipo = models.CharField(max_length=250)

