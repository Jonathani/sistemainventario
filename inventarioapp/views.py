import os
from django.http.response import HttpResponseRedirect
from django.shortcuts import render, redirect, get_object_or_404
from django.views.generic import ListView, DetailView
from django.contrib.auth import authenticate, login as login_process, logout
from django.views.decorators.csrf import csrf_exempt
from django.http import HttpResponseRedirect
from django.http.response import HttpResponseBase
from .models import Cliente
from .models import Bodega
from .models import Categoria
from .models import Producto
from .forms import ClienteForm
from .forms import BodegaForm
from .forms import CategoriaForm


def notfound(request):
    output = '404.html'
    return render(request,output)

def error404(request, *args, **argv):
    output = '404.html'
    return render(request,output)
    
def listarclientes(request):
    if request.user.is_authenticated:
        return render(request, "clientes.html", {"cliente_list":Cliente.objects.all()})
    else:
        return render(request, 'login.html')

def listarbodegas(request):
    if request.user.is_authenticated:
        return render(request, "bodega.html", {"bodega_list":Bodega.objects.all()})
    else:
        return render(request, 'login.html')

def listarcategorias(request):
    if request.user.is_authenticated:
        return render(request, "categoria.html", {"categoria_list":Categoria.objects.all()})
    else:
        return render(request, 'login.html')

class ClienteDetailView(DetailView):
    model = Cliente
    template_name = 'cliente-detail.html'

class BodegaDetailView(DetailView):
    model = Bodega
    template_name = 'bodega-detail.html'

class CategoriaDetailView(DetailView):
    model = Categoria
    template_name = 'categoria-detail.html'

def login(request):
    output = 'login.html'
    return render(request,output)

def index(request):
    if request.user.is_authenticated:
        output = 'index.html'
        return render(request,output)
    else:
        return render(request, 'login.html')

def create(request):
    if request.method == 'POST':
        form = ClienteForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('clientes')
    form = ClienteForm()

    return render(request,'create.html',{'form': form})

def createBodega(request):
    if request.method == 'POST':
        form = BodegaForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('bodega')
    form = BodegaForm()

    return render(request,'create.html',{'form': form})

def createCategoria(request):
    if request.method == 'POST':
        form = CategoriaForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('categoria')
    form = CategoriaForm()

    return render(request,'create.html',{'form': form})

def edit(request, pk, template_name='edit.html'):
    contact = get_object_or_404(Cliente, pk=pk)
    form = ClienteForm(request.POST or None, instance = contact)
    if form.is_valid():
        form.save()
        return redirect('clientes')
    return render(request, template_name, {'form':form})

def editBodega(request, pk, template_name='edit.html'):
    contact = get_object_or_404(Bodega, pk=pk)
    form = BodegaForm(request.POST or None, instance = contact)
    if form.is_valid():
        form.save()
        return redirect('bodega')
    return render(request, template_name, {'form':form})

def editCategoria(request, pk, template_name='edit.html'):    
    contact = get_object_or_404(Categoria, pk=pk)
    form = CategoriaForm(request.POST or None, instance = contact)
    if form.is_valid():
        form.save()
        return redirect('categoria')
    return render(request, template_name, {'form':form})


def delete(request, pk, template_name='confirm_delete.html'):    
    contact = get_object_or_404(Cliente, pk=pk)
    if request.method=='POST':
        contact.delete()
        return redirect('clientes')
    return render(request, template_name, {'object':contact})

def deleteBodega(request, pk, template_name='confirm_delete.html'):
    contact = get_object_or_404(Bodega, pk=pk)
    if request.method=='POST':
        contact.delete()
        return redirect('bodega')
    return render(request, template_name, {'object':contact})

def deleteCategoria(request, pk, template_name='confirm_delete.html'):
    contact = get_object_or_404(Categoria, pk=pk)
    if request.method=='POST':
        contact.delete()
        return redirect('categoria')
    return render(request, template_name, {'object':contact})

def deleteProducto(request, pk, template_name='confirm_delete.html'):
    contact = get_object_or_404(Producto, pk=pk)
    if request.method=='POST':
        contact.delete()
        return redirect('productos')
    return render(request, template_name, {'object':contact})

def ingresarLogin(request):
    message = None    
    # Recuperamos las credenciales y autenticamos al usuario
    usuario = request.POST.get('usuario')
    contrasenia = request.POST.get('contrasenia')
    user = authenticate(request, username=usuario, password=contrasenia)
    # Si es correcto añadimos a la request la información de sesión
    if user is not None:
        login_process(request, user)
        return render(request,"index.html")
    else:
        #  return the response object to Django
        return render(request, 'login.html')

def logout_view(request):
    logout(request)
    return render(request, 'login.html')


def listarProductos(request):
    if request.user.is_authenticated:
        productos = Producto.objects.all()
        bodegas = Bodega.objects.all()
        categorias = Categoria.objects.all()
        clientes = Cliente.objects.all()
        return render(request, "productos.html", {"productos":productos, "bodegas": bodegas, "categorias": categorias, "clientes" : clientes})
    else:
        return render(request, 'login.html')


@csrf_exempt
def createProducto(request):
    if request.method == 'POST':
        codigoProducto = request.POST.get('prodcod')
        nombreProducto = request.POST.get('prodnombre')
        cantidadProducto = request.POST.get('prodcant')
        pCompraProducto = request.POST.get('preciocomp')
        pVentaProducto = request.POST.get('precioventa')
        tipoProducto = request.POST.get('prodtipo')
        categoria = Categoria.objects.get(pk=request.POST.get('categoria'))
        bodega = Bodega.objects.get(pk=request.POST.get('bodega'))
        cliente = Cliente.objects.get(pk=request.POST.get('cliente'))
        #Persistir sobre el objeto producto
        producto = Producto()
        producto.procodigo = codigoProducto
        producto.prodnombre = nombreProducto
        producto.prodcantidad = cantidadProducto
        producto.preciocompra = pCompraProducto
        producto.preciodeventa = pVentaProducto
        producto.prodtipo = tipoProducto
        producto.bogid = bodega
        producto.catid = categoria
        producto.clienteid = cliente
        producto.save()

    template_name = 'productos.html'
    context_object_name = 'productos_list'    
    return redirect('productos')

@csrf_exempt
def createCliente(request):
    if request.method == 'POST':
        cedcli = request.POST.get('cedcli')
        nombrecli = request.POST.get('nombrecli')
        apelcli = request.POST.get('apelcli')
        dircli = request.POST.get('dircli')
        telfcli = request.POST.get('telfcli')
        emailcli = request.POST.get('emailcli')
        #Persistir sobre el objeto cliente
        cliente = Cliente()
        cliente.clicedula = cedcli
        cliente.clinombre = nombrecli
        cliente.cliapellido = apelcli
        cliente.clidireccion = dircli
        cliente.clitelefono = telfcli
        cliente.clicorreo = emailcli
        cliente.save()

    template_name = 'clientes.html'
    context_object_name = 'cliente_list'    
    return redirect('clientes')

@csrf_exempt
def createBodega(request):
    if request.method == 'POST':
        codbod = request.POST.get('codbod')
        nombbod = request.POST.get('nombbod')
        dirbod = request.POST.get('dirbod')
        #Persistir sobre el objeto cliente
        bodega = Bodega()
        bodega.bogcodigo = codbod
        bodega.bognombre = nombbod
        bodega.bogdireccion = dirbod
        bodega.save()

    template_name = 'bodega.html'
    context_object_name = 'bodega_list'    
    return redirect('bodega')

@csrf_exempt
def createCategoria(request):
    if request.method == 'POST':
        catnom = request.POST.get('catnom')
        #Persistir sobre el objeto cliente
        categoria = Categoria()
        categoria.catnom = catnom
        categoria.save()

    template_name = 'categoria.html'
    context_object_name = 'categoria_list'    
    return redirect('categoria')