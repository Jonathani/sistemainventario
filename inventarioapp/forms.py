from django import forms
from django.contrib.auth.forms import UserCreationForm
from .models import Cliente
from .models import Bodega
from .models import Categoria
from .models import Producto

class ClienteForm(forms.ModelForm):
    class Meta:
        model = Cliente
        fields = "__all__"

class BodegaForm(forms.ModelForm):
    class Meta:
        model = Bodega
        fields = "__all__"

class CategoriaForm(forms.ModelForm):
    class Meta:
        model = Categoria
        fields = "__all__"

class ProductoForm(forms.ModelForm):
    class Meta:
        model = Producto
        fields = "__all__"

class LoginForm(forms.ModelForm):
    username = forms.CharField()
    password = forms.CharField()